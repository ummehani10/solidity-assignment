#!/usr/bin/python3
from brownie import SolidityContract, accounts

def main():
    acct = accounts.load('testac')
    return SolidityContract.deploy({'from': acct})