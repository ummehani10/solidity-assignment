### WRITING THE CONTRACT

- Opened remix and created a new file named “MyContract.sol’’ 
- Briefed myself about contracts via : https://www.youtube.com/watch?v=ipwxYa-F1uY
- In constructors added a snippet to set the value of name as “Initial value” so that the value is set as the contract is deployed since the        constructor initializes the contract.
- Created a public function (function name:set) to set the name as the variable (string) is passed to the function.
- Compiled it and deployed it on remix using Javascript VM
- Made one transaction to change the name variable using the public function set
- Checked how I could set a default value for the name instead of calling the constructor and implemented it.
- Complied, Deployed and made another transaction using the set function to verify if everything was working fine.


### DEPLOYING THE CONTRACT

- Googled deploying contract to ethereum testnet using python and read through https://codeburst.io/deploy-a-smart-contract-using-python-how-to-b62de0124b
- Created an environment and set up brownie.
- Initialized brownie and copied the final contract inside the contract folder, cd into folder and compiled the contract.
- Initialized a node to add a host using infura.
- Got an error message : 
- Unable to expand environment variable in host setting: 'https://kovan.infura.io/v3/$WEB3_INFURA_PROJECT_ID'
- Ran command: 
export WEB3_INFURA_PROJECT_ID=aa644db2721b4e089981d306609bc8e0
- Created a test account address using brownie and used the faucet to add eth.
- Deployed using brownie run scripts/deploy.py --network ropsten
- Deployed solidity contract at :  0xE0f331A6AE53bb76e3ed59e72E6CcF99D8f25D4D
https://ropsten.etherscan.io/address/0xE0f331A6AE53bb76e3ed59e72E6CcF99D8f25D4D


### REFERENCES FOR DEPLOYMENT:

- <https://codeburst.io/deploy-a-smart-contract-using-python-how-to-b62de0124b> - initial set up
- <https://www.quicknode.com/guides/web3-sdks/how-to-deploy-a-smart-contract-with-brownie> - test account creation
- <https://eth-brownie.readthedocs.io/en/stable/network-management.html#adding-a-new-network> - Adding infura project ID
- <https://chain.link/bootcamp/brownie-setup-instructions> - infura set up